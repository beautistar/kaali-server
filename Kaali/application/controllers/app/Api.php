<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include("./vendor/autoload.php");

class Api extends CI_Controller {

    

    function __construct(){

        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('app/api_model', 'api_model');
        $this->load->library('session');     
    }
    
    /**
     * Make json response to the client with result code message
     *
     * @param p_result_code : Result code
     * @param p_result_msg : Result message
     * @param p_result : Result json object
     */

    private function doRespond($p_result_code,  $p_result){

         $p_result['result_code'] = $p_result_code;

         $this->output->set_content_type('application/json')->set_output(json_encode($p_result));
    }

    /**
     * Make json response to the client with success.
     * (result_code = 0, result_msg = "success")
     *
     * @param p_result : Result json object
     */

    private function doRespondSuccess($result){

         $this->doRespond(CODE_SUCCESS, $result);
    }

    /**
     * Url decode.
     *
     * @param p_text : Data to decode
     *
     * @return text : Decoded text
     */

    private function doUrlDecode($p_text){

        $p_text = urldecode($p_text);
        $p_text = str_replace('&#40;', '(', $p_text);
        $p_text = str_replace('&#41;', ')', $p_text);
        $p_text = str_replace('%40', '@', $p_text);
        $p_text = str_replace('%20',' ', $p_text);
        $p_text = trim($p_text);


        return $p_text;
    }
    
    function test(){
        echo "API test is OK:)";
    }
    
    function version() {
        phpinfo();
    }
    
    function sendPush($user_id, $type, $body, $content) {
        
        // send FCM push notification
        
        $url = "https://fcm.googleapis.com/fcm/send";
        $api_key = "AAAA_q6-rEA:APA91bFMVI95McE26k71yJV3fzgriMXjVa-fgUdfyozR_7G6-a3b7oWTpzpualrJINg1VWyXb32Rxe0PXg1vKG0XK7T3AhhXf0YvFb07ivJciC4psDq4JWOmW53WX-cJr0f4zsYx0sTc";
        
        $token = "";
        $token = $this->api_model->getToken($user_id);
        
        if (strlen($token) == 0 ) {

            return;
        }
        
//       if(is_array($target)){
//        $fields['registration_ids'] = $target;
//       }else{
//        $fields['to'] = $target;
//           }

        // Tpoic parameter usage
//        $fields = array
//                    (
//                        'to'  => '/topics/alerts',
//                        'notification'          => $msg
//                    );
        $data = array('msgType' => $type,
                      'content' => $content);
        
        $msg = array
                (
                    'body'     => $body,
                    'title'    => 'Transform',   
                    'badge' => 1,             
                    'sound' => 'default'/*Default sound*/
                );
        $fields = array
            (
                //'registration_ids'    => $tokens,
                'to'                => $token,
                'notification'      => $msg,
                'priority'          => 'high',
                'data'              => $data
            );

        $headers = array(
            'Authorization: key=' . $api_key,
            'Content-Type: application/json'
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);

        //@curl_exec($ch);
        
        $result['result'] = curl_exec($ch); 

        curl_close($ch); 
        
        return $result;
        
    }
    
    function stripe_payment() {
        
        $result = array();
        
        $stripe_token = $_POST['stripe_token'];
        $email = $_POST['email'];
        $amount = $_POST['amount'];
        
        try {
            
            \Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);

            $message = \Stripe\Charge::create(array(
              "amount" => $amount,
              "currency" => "usd",
              "source" => $stripe_token, // obtained with Stripe.js
              "description" => "Paid from ".$email
            ));
            
            $result = $message;
            $this->doRespondSuccess($result);
            
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();//`enter code here`
            $err = $body['error'];
            $this->doRespond(PAYMENT_ERROR, $err);
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $body = $e->getJsonBody();
            $err = $body['error'];
            $this->doRespond(PAYMENT_ERROR, $err);
            //return $err['message'];
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $body = $e->getJsonBody();
            $err = $body['error'];
            //return $err['message'];
            $this->doRespond(PAYMENT_ERROR, $err);
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $body = $e->getJsonBody();
            $err = $body['error'];
            //return $err['message'];
            $this->doRespond(PAYMENT_ERROR, $err);
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $body = $e->getJsonBody();
            $err = $body['error'];
            //return $err['message'];
            $this->doRespond(PAYMENT_ERROR, $err);
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $body = $e->getJsonBody();
            $err = $body['error'];
            //return $err['message'];
            $this->doRespond(PAYMENT_ERROR, $err);
        }        
    }
    
    function fb_login() {
        
        $result = array();
        
        $user_name = $_POST['user_name'];
        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];
        $email = $_POST['email'];
        
        $user_id = $this->api_model->exist_user($user_name);
        
        if ($user_id == 0) {
            
            $data = array(
                        'username' => $user_name,
                        'firstname' => $first_name,
                        'lastname' => $last_name,
                        'email' => $this->input->post('email'),                        
                        'created_at' => date('Y-m-d h:m:s'),
                        'updated_at' => date('Y-m-d h:m:s'),
                    );
            $user_id = $this->api_model->register_user($data); 
               
        } else {
            $data = array('updated_at' => date('Y-m-d h:m:s'));
            $this->api_model->update_user($user_id, $data);
        }
        
        $result['user_id'] = $user_id;
        $this->doRespondSuccess($result);
        
    }
    
    function create_event() {
        
        $result = array();
        
        $user_id = $_POST['user_id'];
        $event_name = $_POST['event_name'];
        $event_id = $_POST['event_id'];
        $event_password = $_POST['event_password'];
        $is_approved = isset($_POST['is_approved']) ? $_POST['is_approved'] : 0;
        
        $exist_event = $this->api_model->exist_event($event_id);
        
        if ($exist_event > 0) {
            $this->doRespond(EXIST_EVENT, $result);
        } else {
            $data = array('user_id' => $user_id,
                          'event_name' => $event_name,
                          'event_id' => $event_id,
                          'is_approved' => $is_approved,
                          'event_password' => password_hash($event_password, PASSWORD_BCRYPT),
                          'password' => $event_password,
                          'created_at' => date('Y-m-d h:m:s')
                          );
            $this->api_model->create_event($data);
            $this->api_model->save_event($user_id, $event_id);
            $this->doRespondSuccess($result);  
        }       
              
    }
    
    function login_event() {
        
        $result = array();
        
        $login_userid = $_POST['user_id'];
        $event_id = $_POST['event_id'];
        $event_password = $_POST['event_password'];
                
        $data = array(
        'event_id' => $event_id,
        'event_password' => $event_password
        );
        $result = $this->api_model->login_event($data);
        if ($result == TRUE) {
            $event_data = array(
                'id' => $result['id'],
                'event_id' => $event_id,
                'event_name' => $result['event_name'],
                'user_id' => $result['user_id'],
                'is_approved' => $result['is_approved'],
                'event_password' => $event_password
            );
            $this->api_model->save_event($login_userid, $event_id);
            $this->doRespondSuccess($event_data);
            
        }
        else{
            $this->doRespond(INVALID_LOGIN, array());
        }
    }
    
    function upload_photo() {
         
         $result = array();
         
         $user_id = $_POST['user_id'];
         $event_id = $_POST['event_id'];
         $is_approved = isset($_POST['is_approved'])? $_POST['is_approved'] : 0;
                  
         if(!is_dir("uploadfiles/")) {
             mkdir("uploadfiles/");
         }
         $upload_path = "uploadfiles/";  

         $cur_time = time();
         
         $dateY = date("Y", $cur_time);
         $dateM = date("m", $cur_time);
         
         if(!is_dir($upload_path."/".$dateY)){
             mkdir($upload_path."/".$dateY);
         }
         if(!is_dir($upload_path."/".$dateY."/".$dateM)){
             mkdir($upload_path."/".$dateY."/".$dateM);
         }
         
         $upload_path .= $dateY."/".$dateM."/";
         $upload_url = base_url().$upload_path;

        // Upload file. 

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => MAX_WIDTH,
            'max_height' => MAX_HEIGHT,
            'file_name' => $event_id.'_'.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('file')) {       

            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            $data = array('user_id' => $user_id,
                          'event_id' => $event_id,
                          'photo_url' => $file_url, 
                          'is_approved' => $is_approved, 
                          'upload_at' => date('Y-m-d h:m:s')
                          );
            $id = $this->api_model->upload_photo($data);
            $result['upload_url'] = $file_url;
            $this->doRespondSuccess($result);

        } else {

            $this->doRespond(UPLOAD_FAIL, $result);// upload fail
            return;
        }     
    }
    
    function get_photos(){
        
        $result = array();
        
        $event_id = $_POST['event_id'];
        
        $result['event_photos'] = $this->api_model->get_event_photos($event_id);
        
        $this->doRespondSuccess($result);
    }
    
    function get_prices() {
        
        $result = array();
        
        $result = $this->api_model->get_prices();
        
        $this->doRespondSuccess($result);
    }
    
    function send_order() {
        
        $result = array();        
        $request = array();


        $data = json_decode(file_get_contents('php://input'), true);

        $request = $data["order_list"];

        foreach($request as $order) {
            
            $data =  array( 'photo_id' => $order['photo_id'],
                            'comment' => $order['comment'],
                            'user_id' => $order['user_id'],
                            'email' => $order['email'],
                            'transaction_id' => $order['transaction_id'],
                            'event_id' => $order['event_id'],
                            'created_at' => date('Y-m-d h:m:s'));
            $this->api_model->add_order($data);
        }        
            
        $this->doRespondSuccess($result);
        
    }
    
    function register_user() {
        
        $result = array();
        
        $user_name = $_POST['user_name'];
        $password = $_POST['password'];
        
        $exist_user = $this->api_model->exist_user($user_name);
        
        if ($exist_user > 0) {
            $this->doRespond(EXIST_USER, $result);
        } else {
            $data = array(
                        'username' => $user_name,
                        'password' => password_hash($password, PASSWORD_BCRYPT),
                        'created_at' => date('Y-m-d h:m:s'),
                        'updated_at' => date('Y-m-d h:m:s'),
                    );
            $user_id = $this->api_model->register_user($data);
            $result['user_id'] = $user_id;
            $this->doRespondSuccess($result);  
        }        
    }
    
    function login_user() {
        
        $result = array();
        
        $user_name = $_POST['user_name'];
        $password = $_POST['password'];
                
        $data = array(
            'username' => $user_name,
            'password' => $password
        );
        
        $result = $this->api_model->login_user($data);
        if ($result == TRUE) {
            $user_data = array(
                'user_id' => $result['id'],
                'user_name' => $result['username']                
            );
            
            $this->doRespondSuccess($user_data);
        }
        else{
            $this->doRespond(INVALID_LOGIN, array());
        }
    }
    function approve_photo() {
        
        $result = array();
        $photo_id = $_POST['photo_id'];
        $this->api_model->approve_photo($photo_id);
        $this->doRespondSuccess($result);
    }
    
    function get_event() {
        
        $result = array();
        $t_result = array();
        $user_id = $_POST['user_id'];
        $q_result = $this->api_model->get_event($user_id);
        foreach ($q_result as $row) {
            $event = $this->get_event_one($row);
            array_push($t_result, $event);
        } 
        
        $result['event_list'] = $t_result;
        $this->doRespondSuccess($result);        
        
    }
    
    function get_event_one($array) {
        
        $object = array('id' => $array['id'],                             
                        'user_id' => $array['user_id'],
                        'event_name' => $array['event_name'],
                        'event_id' => $array['event_id'],
                        'event_password' => $array['password'],
                        'is_approved' => $array['is_approved'],
                        'created_at' => $array['created_at']
                        );
        return $object;
        
    }   
    
}
?>
